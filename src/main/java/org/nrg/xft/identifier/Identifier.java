//Copyright 2005 Harvard University / Howard Hughes Medical Institute (HHMI) All Rights Reserved
/* 
 * XDAT � Extensible Data Archive Toolkit
 * Copyright (C) 2005 Washington University
 */
/*
 * Created on Jan 10, 2005
 *
 */
package org.nrg.xft.identifier;

/**
 * @author Tim
 *
 */
public interface Identifier {
	
	/**
	 * @return String which uniquely identifies this object (id or name)
	 */
	public String getId();
}

