// Copyright 2010 Washington University School of Medicine All Rights Reserved
package org.nrg.xft.event;


public interface EventListener {
	public void handleEvent(Event e) throws Exception;
}
